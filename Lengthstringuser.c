
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
#include<stdio.h>
 
// Prototype Declaration
int FindLength(char str[]);
 
int main() {
   char str[100];
   int length;
 
   printf("\nEnter the String : ");
   gets(str);
 
   length = FindLength(str);
 
   printf("\nLength of the String is : %d", length);
   return(0);
}
 
int FindLength(char str[]) {
   int len = 0;
   while (str[len] != '\0')
      len++;
   return (len);
}