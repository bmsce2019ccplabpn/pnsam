#include <stdio.h>
 
int main()
{
   int array[100], position, c, n, value;
 
   printf("Enter number of elements in array\n");
   scanf("%d", &n);
 
   printf("Enter %d elements\n", n);
 
   for (c = 0; c < n; c++)
      scanf("%d", &array[c]);
 
   printf("Enter the location where you wish to insert an element\n");
   scanf("%d", &position);
 
   printf("Enter the value to insert\n");
   scanf("%d", &value);
 
   for (c = n - 1; c >= position - 1; c--)
      array[c+1] = array[c];
 
   array[position-1] = value;
 
   printf("Resultant array is\n");
 
   for (c = 0; c <= n; c++)
      printf("%d\n", array[c]);
 
   return 0;
}

 
Output of program:
 C program output to insert an element in an array

Download Insert element in array program.

C programs
C programming
C graphics
C graphics programs
C source codes
Tutorials
Data structures
C mouse programs
C++ programs
Java programs
conio.h
math.h
Programming books

 
C programs
Hello world
Print Integer
Addition
Odd or Even
Add, subtract, multiply and divide
Check vowel
Quadratic equation roots
Leap year
Add digits
Factorial
HCF and LCM
Decimal to binary conversion
nCr and nPr
Add n numbers
Swapping
Reverse number
Palindrome number
Print Pattern
Diamond
Prime numbers
Find armstrong number
Generate armstrong number
Fibonacci series
Print Floyd's triangle
Print Pascal triangle
Addition using pointers
Maximum element in array
Minimum element in array
Linear search
Binary search
Reverse array
Insert element in array
Delete element from array
Merge arrays
Bubble sort
Insertion sort
Selection sort
Add matrices
Subtract matrices
Transpose matrix
Multiply two matrices
Print string
String length
Compare strings
Copy string
Concatenate strings
Reverse string
Find palindrome
Delete vowels
C substring
Subsequence
Sort a string
Remove spaces
Change case
Swap strings
Character's frequency
Anagrams
Read file
Copy files
Merge two files
List files in a directory
Delete file
Random numbers
Add complex numbers
Print date
Get IP address
Shutdown computer


 
Programming Simplified is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

Home | About | Contact | Programmer Resources | Sitemap | Privacy | Facebook

